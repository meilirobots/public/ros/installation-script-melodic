#!/bin/bash
# Get pin and path
pin="$1"
echo "pin: $1";
THISPATH=`pwd`

# Preventing sudo timeout https://serverfault.com/a/833888
trap "exit" INT TERM; trap "kill 0" EXIT; sudo -v || exit $?; sleep 1; while true; do sleep 60; sudo -nv; done 2>/dev/null &

# Common dependencies
echo "Installing common dependencies"
sudo apt-get update -y
sudo apt-get install git 

# Required python packages
sudo apt-get install python3-pip -y
echo "install pip"
sudo -H pip3 install --upgrade pip
echo "pip upgraded"
sudo apt-get install ros-melodic-catkin
sudo apt-get install python-rosdep
sudo apt-get install python3-catkin-pkg-modules -y
sudo apt-get install python3-rospkg-modules -y
 
# Creating catkin workspace
echo "creating a ros workspace"
mkdir -p ~/catkin_ws_meili/src
cd ~/catkin_ws_meili
catkin_make
export LAST_CATKIN_PATH=`pwd`'/src'
cd $THISPATH
export ROS_PACKAGE_PATH=$LAST_CATKIN_PATH:$ROS_PACKAGE_PATH

# Download meili agent using meili-cli and pin from Meili FMS
echo "Downloading meili_agent using meili-cli"
./meili-cli init -pin $pin
./meili-cli setup
cd ~/catkin_ws_meili/src/meili-agent/src/

# Install python packages
sudo -H pip3 install --ignore-installed PyYAML
sudo -H pip3 install -r requirements.txt
sudo -H pip3 install typing_extensions
sudo apt-get update

cd ~/catkin_ws_meili/

# Setup environment variables

rossource="source /opt/ros/melodic/setup.bash"
if grep -Fxq "$rossource" ~/.bashrc; then echo ROS setup.bash already in .bashrc;
else echo "$rossource" >> ~/.bashrc; fi
eval $rossource
catkin_ws_meili_source="source ~/catkin_ws_meili/devel/setup.bash"
eval $catkin_ws_meili_source

## Update rosdep and install Meili agent ros dependencies
sudo rosdep init
rosdep update
rosdep install --from-paths src --ignore-src --rosdistro melodic -y -r

## Build catkin ws
catkin_make

## Re-source environment to reflect new packages/build environment
if grep -Fxq "$catkin_ws_meili_source" ~/.bashrc; then echo ROS catkin_ws setup.bash already in .bashrc; 
else echo "$catkin_ws_meili_source" >> ~/.bashrc; fi
eval $catkin_ws_meili_source
source ~/.bashrc
echo "meili_agent installed in $LAST_CATKIN_PATH"
